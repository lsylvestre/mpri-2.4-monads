(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Base = struct
  type 'a t =
    | Val of 'a
    | Err of exn

  let return a = Val a

  let bind m f =  
    match m with 
    | Val x -> f x
    | Err e -> Err e
end

module M = Monad.Expand (Base)
include M
open Base

let err e = Err e

let try_with_finally m ks kf = 
  match m with
  | Val x -> ks x
  | Err e -> kf e

let run m = 
  match m with 
  | Val x -> x
  | Err e -> raise e

